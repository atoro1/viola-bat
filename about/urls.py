from django.urls import path
from about.views import AboutHome


app_name = "about"
urlpatterns = [
    path("", AboutHome.as_view(), name="home")
]
