from django.urls import path
from commissions import views


app_name = "commissions"
urlpatterns = [
    path("", views.CommissionsHome.as_view(), name="home"),
]
