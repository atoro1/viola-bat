from django.views import generic
from pages import models as page_models


class CommissionsHome(generic.TemplateView):
    template_name = "pages/page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = page_models.Page.objects.get(slug="commissions")
        return context
