from django.contrib import admin
from gallery import models


@admin.register(models.GalleryImage)
class GalleryImageAdmin(admin.ModelAdmin):
    pass


@admin.register(models.GalleryCategory)
class GalleryCategoryAdmin(admin.ModelAdmin):
    pass
