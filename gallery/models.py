from django.db import models
from taggit.managers import TaggableManager


class GalleryImage(models.Model):
    image = models.ImageField()
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    order = models.PositiveSmallIntegerField(default=0)
    tags = TaggableManager()
    categories = models.ManyToManyField("GalleryCategory")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["order", "id"]


class GalleryCategory(models.Model):
    name = models.CharField(max_length=50)
    order = models.PositiveSmallIntegerField(default=0)

    class Meta:
        verbose_name_plural = "gallery categories"

    def __str__(self):
        return self.name
