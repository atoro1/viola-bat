from django.views import generic
from gallery import models
from taggit.models import Tag


class GalleryHome(generic.ListView):
    model = models.GalleryImage
    template_name = "gallery_base.html"
    paginate_by = 24

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tag_options"] = list(Tag.objects.values_list("name", flat=True))
        context["selected_tags"] = self.request.GET.getlist("tag")
        context["any_all_tags"] = self.request.GET.get("search") == "all"
        print(self.request.GET.getlist("tag"))
        return context

    def get_queryset(self):
        queryset = models.GalleryImage.objects.all()
        if len(self.request.GET.getlist("tag")) == 0:
            return queryset
        if self.request.GET.get("search") == "all":
            for tag in self.request.GET.getlist("tag"):
                queryset = queryset.filter(tags__name=tag)
        else:
            queryset = queryset.filter(tags__name__in=self.request.GET.getlist("tag")).distinct()
        return queryset
