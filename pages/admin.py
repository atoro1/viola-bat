from django.contrib import admin
from pages import models


@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'order')


@admin.register(models.Option)
class OptionAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')
