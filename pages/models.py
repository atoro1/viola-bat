from django.db import models
from django_quill.fields import QuillField
from django.utils.text import slugify


class Page(models.Model):
    """
    A single web page
    """
    title = models.CharField(max_length=50)
    slug = models.SlugField(null=True, unique=True)
    order = models.PositiveSmallIntegerField(default=0)
    content = QuillField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


class Option(models.Model):
    """
    An option model to add site-wide variables
    """
    name = models.CharField(max_length=50)
    value = models.TextField()
