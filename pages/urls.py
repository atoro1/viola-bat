from django.urls import path
from pages import views


app_name = "pages"
urlpatterns = [
    path("<slug:slug>/", views.PageView.as_view(), name="page"),
]
