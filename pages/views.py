from django.views import generic
from pages import models


class PageView(generic.DetailView):
    template_name = "pages/page.html"
    model = models.Page
