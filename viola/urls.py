from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from viola import views

admin.site.site_title = "Viola Bat"
admin.site.site_header = "Viola Bat administration"

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", views.HomePageView.as_view(), name="home"),
    path("commissions/", include("commissions.urls", namespace="commissions")),
    path("gallery/", include("gallery.urls", namespace="gallery")),
    path("pages/", include("pages.urls", namespace="pages")),
    path("about/", include("about.urls", namespace="about")),
    path('__debug__/', include('debug_toolbar.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
